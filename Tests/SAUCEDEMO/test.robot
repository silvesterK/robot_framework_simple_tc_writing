*** Settings ***
Documentation    Tests to verify that user can login with valid credentials,
...              filter items depending on price highest to lowest,
...              add an item to cart and logout.

Library    SeleniumLibrary
Resource   ../../Resources/SAUCEDEMO/Locators.robot
Resource   ../../Resources/SAUCEDEMO/Common.robot
Resource   ../../Resources/SAUCEDEMO/Variables.robot
Suite Setup    Test suite pre run
Suite Teardown    Test suite post run

*** Test Cases ***
Login With Valid Credentials
    Login    ${VALID_USERNAME}    ${VALID_PASSWORD}
    Verify that product page is open

Buy most expencive item then check cart
    Add most expencive item to cart and save label
    Check shopping cart has item    ${ADDED_ITEM}

Logout and verify
    Logout
    Verify that login page is open

*** Keywords ***
Login
    [Arguments]  ${USERNAME}  ${PASSWORD}
    Enter username  ${USERNAME}
    Enter password  ${PASSWORD}
    Click login button


Logout
    Click Element  ${PRODUCT_PAGE_MENU_BTN}
    Wait Until Element Is Visible  ${MENU_DIALOG_LOGOUT_BTN}  timeout=5s
    Click Element  ${MENU_DIALOG_LOGOUT_BTN}

Verify that login page is open
    Wait Until Element Is Visible  ${LOGIN_PAGE_LOGIN_BTN}  timeout=5s
    Wait Until Element Is Visible  ${LOGIN_PAGE_BOT_LOGO}  timeout=5s

Verify that product page is open
    Wait Until Element Is Visible  ${PRODUCT_PAGE_MENU_BTN}  timeout=5s

Filter items by price high to low
    Filter items by  ${PRODUCT_PAGE_FILTER_PRICE_HIGH_TO_LOW}

Add most expencive item to cart and save label
    Filter items by price high to low
    Click top item add to cart button
    ${TOP_ELEMENT_LABEL_TEXT}=   Get top element label
    Set Test Variable  ${ADDED_ITEM}  ${TOP_ELEMENT_LABEL_TEXT}

Check shopping cart has item
    [Arguments]  ${ITEM}
    Click shopping cart button
    Check shopping cart list has item  ${ITEM}
