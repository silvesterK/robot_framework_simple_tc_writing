*** Variables ***

#*** SAUCEDEMO_LOACORS ***

#*** LOGIN_PAGE ***
${LOGIN_PAGE_BOT_LOGO}    class:bot_column
${LOGIN_PAGE_USERNAME_TEXTFIELD}    id:user-name
${LOGIN_PAGE_PASSWORD_TEXTFIELD}    id:password
${LOGIN_PAGE_LOGIN_BTN}   id:login-button

#*** PRODUCT_PAGE ***
${PRODUCT_PAGE_FILTER_DROPDOWN}   class:product_sort_container
${PRODUCT_PAGE_TOP_ITEM_ADD_TO_CART_BTN}    xpath=((//*[@class="inventory_item"])[1]//*[contains(text(),"Add to cart")])
${PRODUCT_PAGE_TOP_ITEM_LABEL}    xpath=((//*[@class="inventory_item_name"])[1])
${PRODUCT_PAGE_SHOPPING_CART_BTN}   class:shopping_cart_link
${PRODUCT_PAGE_MENU_BTN}    id:react-burger-menu-btn

#*** PRODUCT_PAGE_MENU_DIALOG ***
${MENU_DIALOG_LOGOUT_BTN}    id:logout_sidebar_link

#*** SHOPPING_CART_PAGE ***
${SHOPPING_CART_PAGE_LIST}   class:cart_list