*** Settings ***

Library    SeleniumLibrary
Resource   ../SAUCEDEMO/Locators.robot
Resource   ../SAUCEDEMO/Variables.robot

*** Keywords ***
Test suite pre run
    Open Browser  ${SAUCEDEMO_URL}  ${BROWSER}
    Maximize browser window
    Verify that login page is open

Test suite post run
    Close the browser

Close the browser
    Run Keyword If All Tests Passed  close browser
    Run Keyword If Any Tests Failed  close browser

Enter username
    [Arguments]  ${USERNAME}
    Wait Until Element Is Visible  ${LOGIN_PAGE_USERNAME_TEXTFIELD}  timeout=5s
    Input Text  ${LOGIN_PAGE_USERNAME_TEXTFIELD}  ${USERNAME}

Enter password
    [Arguments]  ${PASSWORD}
    Wait Until Element Is Visible  ${LOGIN_PAGE_PASSWORD_TEXTFIELD}  timeout=5s
    Input Text  ${LOGIN_PAGE_PASSWORD_TEXTFIELD}  ${PASSWORD}

Click login button
    Wait Until Element Is Visible  ${LOGIN_PAGE_LOGIN_BTN}  timeout=5s
    Click Element  ${LOGIN_PAGE_LOGIN_BTN}

Click shopping cart button
    Wait Until Element Is Visible  ${PRODUCT_PAGE_SHOPPING_CART_BTN}  timeout=5s
    Click Element  ${PRODUCT_PAGE_SHOPPING_CART_BTN}

Check shopping cart list has item
    [Arguments]  ${ITEM}
    Wait Until Element Is Visible  ${SHOPPING_CART_PAGE_LIST}  timeout=5s
    wait until element contains  ${SHOPPING_CART_PAGE_LIST}  ${ITEM}  timeout=5s

Filter items by
    [Arguments]  ${FILTER_VALUE}
    Wait Until Element Is Visible  ${PRODUCT_PAGE_FILTER_DROPDOWN}  timeout=5s
    Select From List By Label  ${PRODUCT_PAGE_FILTER_DROPDOWN}  ${FILTER_VALUE}

Click top item add to cart button
    Wait Until Element Is Visible  ${PRODUCT_PAGE_TOP_ITEM_ADD_TO_CART_BTN}  timeout=5s
    Click Element  ${PRODUCT_PAGE_TOP_ITEM_ADD_TO_CART_BTN}

Get top element label
    ${TOP_ELEMENT_LABEL_TEXT}=  Get text  ${PRODUCT_PAGE_TOP_ITEM_LABEL}
    [Return]    ${TOP_ELEMENT_LABEL_TEXT}



