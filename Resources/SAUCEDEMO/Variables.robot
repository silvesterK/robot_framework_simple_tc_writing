*** Variables ***

#*** SAUCEDEMO_VARIABLES ***

#*** GENERAL ***
${BROWSER}    chrome
${SAUCEDEMO_URL}    https://www.saucedemo.com/

#*** LOGIN_PAGE ***
${VALID_USERNAME}    standard_user
${VALID_PASSWORD}    secret_sauce

#*** PRODUCT_PAGE ***
${PRODUCT_PAGE_FILTER_PRICE_HIGH_TO_LOW}   Price (high to low)
${PRODUCT_PAGE_FILTER_PRICE_LOW_TO_HIGH}   Price (low to high)
${PRODUCT_PAGE_FILTER_NAME_A_TO_Z}   Name (A to Z)
${PRODUCT_PAGE_FILTER_NAME_Z_TO_A}   Name (Z to A)